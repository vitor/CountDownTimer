package com.ep.countdowntimer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ep.countdowntimer.timer.CountDownTimerImpl;

public class MainActivity extends AppCompatActivity {
    private TextView mTextView;
    private  CountDownTimerImpl mCountDownTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView= (TextView) findViewById(R.id.tv_getSecurityCode);
        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountDownTimer=new CountDownTimerImpl(60000,1000,mTextView);
                mCountDownTimer.start();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCountDownTimer.cancel();
    }
}
